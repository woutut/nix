#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Первый класс
class FootballClub:
    def __init__(
        self, won_games=0, draw_games=0, lost_games=0, goals=0, opponents_goals=0
    ):
        self.won_games = won_games
        self.draw_games = draw_games
        self.lost_games = lost_games
        self.goals = goals
        self.opponents_goals = opponents_goals

    # Добавление данных о проведенном мачте
    def add_match_result(self, goals, opponents_goals):
        # Добавляю данные по голам
        self.goals += goals
        self.opponents_goals += opponents_goals
        # Обновляю статистику матчей
        if goals > opponents_goals:
            self.won_games += 1
        elif goals == opponents_goals:
            self.draw_games += 1
        else:
            self.lost_games += 1
        # Вывожу данные в консоль для отладки/проверки
        print(
            'Добавлен результат матча: {} забито, {} пропущено'.format(
                goals, opponents_goals
            )
        )

    # Подсчёт количества очков
    def count_points(self):
        return self.won_games * 3 + self.draw_games * 1

    # Подсчёт разницы забитых и пропущенных голов
    def count_goal_difference(self):
        return self.goals - self.opponents_goals


# Второй класс (child)
class FootbalClubUpgraded(FootballClub):

    # Подсчёт общего количества игр
    def count_club_games(self):
        return self.won_games + self.draw_games + self.lost_games


# Если файл выполняется напрямую:
if __name__ == "__main__":

    # Создаю сущность класса
    # Child класс включает в себя все аргументы/родительского
    # Потому тестировать буду на нём
    club = FootbalClubUpgraded()

    # Вывожу данные по голам клуба
    print(
        'Забитые голы: {}, Пропущенные голы: {}'.format(
            club.goals, club.opponents_goals
        )
    )

    # Добавляю в историю победный матч
    club.add_match_result(3, 1)

    # Вывожу данные по голам клуба
    print(
        'Забитые голы: {}, Пропущенные голы: {}'.format(
            club.goals, club.opponents_goals
        )
    )

    # Добавляю в историю ничью
    club.add_match_result(1, 1)

    # Вывожу данные по голам клуба
    print(
        'Забитые голы: {}, Пропущенные голы: {}'.format(
            club.goals, club.opponents_goals
        )
    )

    # Считаю очки клуба (должно быть 4, 1 победа, 1 ничья)
    print('Очки клуба: {}'.format(club.count_points()))

    # Считаю разницу голов клуба (должно быть 2, 4 забито, 2 пропущено)
    print('Разница голов: {}'.format(club.count_goal_difference()))

    # Считаю общее количество игр (должно быть 2, 1 победа, 1 ничья)
    print('Всего игр сыграно: {}'.format(club.count_club_games()))
