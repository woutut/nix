#!/usr/bin/env python
# -*- coding: utf-8 -*-s
import requests
import json
from tqdm import tqdm
from pprint import pprint


# Функция получения всех кодов языков
def get_all_language_codes(data_url):

    # Запрашиваю данные
    countries_data = requests.get(
        data_url, headers={'User-Agent': 'Python Learning Requests'}
    )

    # Обрабатываю в JSON
    countries = json.loads(countries_data.text)

    # Подготавливаю set для хранения уникальных языков
    unique_languages = set()

    # Собираю языки по странам
    for c in countries:
        # Обрабатываю языки
        for l in c.get('languages', []):
            unique_languages.add(l)

    # Возвращаю уникальные языки
    return unique_languages


# Функция сбора данных по населению по языкам
def get_language_population(languages, data_url):

    # Подготавливаю словарь для хранения данных по наслению
    population_data = {x: 0 for x in languages}

    # Обрабатываю языки
    for code in tqdm(languages, desc='Обработка языков'):
        # Запрашиваю данные по языку
        code_data = requests.get(
            '{}{}'.format(data_url, code),
            headers={'User-Agent': 'Python Learning Requests'},
        )
        # Обрабатываю в JSON
        code_countries = json.loads(code_data.text)
        # Обрабатываю страны
        for country in code_countries:
            # Добавляю население к языку
            population_data[code] += country.get('population', 0)

    # Возвращаю словарь с населением
    return population_data


# Если файл выполняется напрямую:
if __name__ == "__main__":

    language_codes = get_all_language_codes('https://restcountries.eu/rest/v1/all')
    print(language_codes)

    language_population = get_language_population(
        language_codes, 'https://restcountries.eu/rest/v1/lang/'
    )
    pprint(language_population)
