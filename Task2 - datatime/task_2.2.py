#!/usr/bin/env python
# -*- coding: utf-8 -*-s
from datetime import datetime


# Функция возврата знака зодиака
def get_zodiac(birth):

    # Подготавливаю данные по знакам зодиака в md-формате
    # Первым числом идёт месяц, за ним двухчисельная дата (01, 23, и т.п.)
    zodiacs = [
        (120, 'Козерог'),
        (218, 'Водолей'),
        (320, 'Рыбы'),
        (420, 'Овен'),
        (521, 'Телец'),
        (621, 'Близнецы'),
        (722, 'Рак'),
        (823, 'Лев'),
        (923, 'Дева'),
        (1023, 'Весі'),
        (1122, 'Скорпион'),
        (1222, 'Стрелец'),
        (1231, 'Козерог'),
    ]

    # Распознаю дату
    date_of_birth = datetime.strptime(birth, "%d-%m-%Y").date()

    # Конвертирую дату в mdd-формат
    date_number = int('{}{:0>2d}'.format(date_of_birth.month, date_of_birth.day))

    # Обрабатываю знаки от раннего к позднему
    for zd in zodiacs:
        # Если дата попадает в промежуток
        if date_number <= zd[0]:
            # Возвращаю знак
            return zd[1]


# Если файл выполняется напрямую:
if __name__ == "__main__":
    date = '11-05-1991'
    print(get_zodiac(date))
