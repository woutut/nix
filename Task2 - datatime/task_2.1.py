#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import date, timedelta

# Список для хранения пятниц
fridays_the_thirteenth = []

# Определяю ближайшую пятницу
next_friday = date.today() + timedelta(days=(4 - date.today().weekday()) % 7)

# Ищу следующие, вношу список, если 13-е
while len(fridays_the_thirteenth) < 10:
    # Добавляю неделю
    next_friday += timedelta(days=7)
    # Проверяю, 13-е ли
    if next_friday.day == 13:
        fridays_the_thirteenth.append(next_friday)


# Если файл выполняется напрямую:
if __name__ == "__main__":
    print(fridays_the_thirteenth)
