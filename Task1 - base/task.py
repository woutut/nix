#!/usr/bin/env python
# -*- coding: utf-8 -*-s
import re
from deepdiff import DeepDiff
from file_read_backwards import FileReadBackwards


# Функция обработки eid в словарь
def eid_str_to_dict(eid_str):
    if eid_str:
        return {
            y.split('.')[0]: y.split('.')[1] for y in [x for x in eid_str.split(';')]
        }
    else:
        return {}


# Функция вывода последних изменений в eid
# По умолчанию считываю с конца файла
def get_last_eids(file_path, reverse=True):

    # Подготавливаю список для хранения изменений eid
    eid_changes = []

    # Реализация в двух вариантах - считывание с конца и с начала файла
    # В условиях задачи нет уточнения как именно, потому сделаю обе реализации

    # Считывать с конца правильнее, потому что логфайлы могут быть огромными
    # и правильнее использовать генераторы, ибо reverse(list(file)) может
    # не поместиться в память, в данном случае использую модуль file_read_backwards
    if reverse:
        # Открываю файл
        with FileReadBackwards(file_path) as logfile:
            # Ищу данные пока не найду 2 последних записи
            while len(eid_changes) < 2:
                # Обрабатываю записи лога
                for line in logfile:
                    # Ищу eid в записи
                    eid = re.findall('.*SetUniqueParam\([0-9]*\): eid: (.*)', line)
                    # Если нахожу - добавляю в список
                    if eid:
                        eid_changes.append(eid[0])
        # Меняю порядок в файле на очевидный (предпоследний первым, последний вторым)
        eid_changes = eid_changes[::-1]

    # Стандартный способ
    else:
        # Открываю файл
        with open(file_path, 'r') as logfile:
            # Ищу данные пока не найду 2 последних записи
            while len(eid_changes) < 2:
                # Обрабатываю записи лога
                for line in logfile:
                    # Ищу eid в записи
                    eid = re.findall('.*SetUniqueParam\([0-9]*\): eid: (.*)', line)
                    # Если нахожу - добавляю в список
                    if eid:
                        eid_changes.append(eid[0])

    # Обрабатываю данные в словарь и вывожу данные
    penult_eid = eid_str_to_dict(eid_changes[-2])
    last_eid = eid_str_to_dict(eid_changes[-1])
    print('Предпоследние eid: {}'.format(penult_eid))
    print('*' * 50)
    print('Последние eid: {}'.format(last_eid))
    print('*' * 50)

    # Возвращаю отличия
    return DeepDiff(penult_eid, last_eid)


# Если файл выполняется напрямую:
if __name__ == "__main__":
    print(get_last_eids('for_test.log'))
    print(get_last_eids('for_test.log', reverse=False))
